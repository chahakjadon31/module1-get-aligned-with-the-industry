**CONVOLUTIONAL NEURAL NETWORK**

* CNN treats data as spatial (_spatial domain techniques directly deal with the image pixels_)
* CNN simplifies the image so that it can be better processed and understood.
* CNN is used for classification images.

**Key Notes**
* **Convolutional Layer**: It works by placing a filter over an array of image pixels. This then creates what's called a convolved feature map.
* **Pooling layer**: This down samples or reduces the sample size of a articular faeture map. The ouput of this is a pooled feature map.
* **ReLU** (_Rectified Linear Unit Layer_): It acts as an activation function. 
* **Fully connected Layer**: It allows you to perform classification on your data set.

Structure and steps to classify the output with image as an input:

[Steps for CNN](https://docs.google.com/document/d/1kJbqp5pKKBB5aPZuTV5jXRPPVF_xkCdSgcJaciUm6Cs/edit)


