**Product Name 1**
Scene Content Classification (Visual Recognition)

**Product Link**
[link](https://www.researchgate.net/publication/220875360_Industry_and_Object_Recognition_Applications_Applied_Research_and_Challenges)

**Product Short Description**

Knowledge of scene content is used to improve
* stabilization
* moving object detection
* track loss due to occlusion


**Product is combination of features**
* Object recognition
* Segmentation

**Product is provided by which company**
General Electric Global Research


**Product Name 2**

Autism & Beyond App

**Product Link**
[Link](https://www.cbinsights.com/research/facial-recognition-disrupting-industries/)


**Product Short Description**

This app uses iphones front camera and facial recogbition algorityms to screen children for autism.



**Product is combination of features**
* Face Recognition
* Face detection

**Product is provided by which company**
Researchers at Duke University