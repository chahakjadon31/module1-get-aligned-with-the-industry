**DEEP NEURAL NETWORK**

**Plain Vanilla/ Multilayer perceptron**

Here, we are learning to recognize hand written digits, the image used is of _28*28_ pixels.

>**STRUCTURE OF DNN**

**Structure of the hand written neural network** [**Structure**](https://docs.google.com/document/d/1TgYBX3lmwptTyHQG3sBj534hFh3WGuJ0-PqQKEkRjS0/edit)

**Key Points**
 * Activation in the last layer's neuron represents how much the system thinks that the given image corresponds with the given digit.
 * Activation in one layer determines the activation in the next layer.


_Assuming that the first hidden layer corresponds to various little edges and the second hidden layer corresponds to the patterns of the digits._

[Parameters with structure](https://docs.google.com/document/d/178u3-5cns__bx_J83zWRdFaSJJj0pHDbVoWIGq9vLqw/edit)

 **PARAMETERS**
 1. **Weights**
    * It tells you what pixel pattern the neuron in the 2nd layer is picking on
    * Assigned to each one of the connections between our neuron and neuron from the 1st layer.
2. **Bias**
    * Each neuron has its own bias
    * It tells you how high the weighted sum needs to be before neuron starts gettig meaningfully active. 


>**LEARNING OF DNN**

_Finding the right weights and biases and minimizing the cost function_

**Neural network function**
 * Input: 784 numbers
 * Output: 10 numbers
 * Parameters: 13,002 weights & biases

 **Cost Function**
 * Input: 13,002 weights & biases
 * Output: 1 number (the cost)
 * Parameters: many, many, many traiining examples

 **Gradient** of a function gives you the direction of the steepest increase (ascent) and negative of this gradient gives the steepest descent.

[Algorithm for minimizing the function](https://docs.google.com/document/d/1eNUU23_s880XHAW3HTKzzktwvAXUWgv8Vld97qeAw24/edit)

**Gradient Vector of Cost Function**: It encodes the realtive importance of each weight and bias. (_tells us what nudges to all the weights and biases cause the fastest change to the value of the cost function_)

>**BACK PROPAGATION**

_Core algorithm behind how neural networks learn, computing the gradient_

When the network is not well trained, so the computer's output is wrong. Activations in the computers output are going to be pretty random. We can't change activations, we only have **influence over weights and biases**.

[How to increase the activation?](https://docs.google.com/document/d/1loh8YalOv7uRVAUeuPcvVwFzhtCSZ4ywKs2ZlIMHZaI/edit)

**Key Points**
* The connections with the brightest neurons from the preceding layer have the biggest effect. Therefore, they have stronger influence on cost function.
*  In propogation to the corresponding weights and to how much each of the neuron needs to change. This is where the idea of **propogating backwards** comes in. 

**Stochastic gradient descent**
* It takes longer time for computers to add up every single gradient descent step, so the process in divided.
* Randomnly shuffle your training data
* Divide it into a whole bunch of mini-batches
* Compute gradient descent(using backprop)















